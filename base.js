'use strict';

module.exports = {
	extends: 'xo',
	rules: {
		curly: [
			'error',
			'multi-line',
		],
		'object-curly-spacing': [
			'error',
			'always',
		],
		eqeqeq: [
			'error',
			'allow-null',
		],
		'no-eq-null': [
			'off',
		],
		'comma-dangle': [
			'error',
			'always-multiline',
		],
		'prefer-const': [
			'error',
		],
		'no-var': [
			'error',
		],
		'max-len': [
			'warn',
			{
				code: 140,
				tabWidth: 4,
				ignoreUrls: true,
			},
		],
		'capitalized-comments': [
			'off',
		],
		'no-prototype-builtins': [
			'off',
		],
		indent: ['error', 'tab', {
			SwitchCase: 1,
			MemberExpression: 0,
		}],
		'one-var': [
			'off',
		],
		'one-var-declaration-per-line': [
			'error',
			'initializations',
		],
		'space-before-function-paren': [
			'error',
			{
				anonymous: 'never',
				named: 'never',
				asyncArrow: 'always',
			},
		],
	},
};
