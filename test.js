'use strict';

const path = require('path');

module.exports = {
	extends: path.join(__dirname, './index.js'),
	globals: {
		// Mocha
		describe: true,
		it: true,
		before: true,
		after: true,
		beforeEach: true,
		afterEach: true,
		// Custom
		assert: true,
		expect: true,
		runTests: true,
		sinon: true,
	},
};
