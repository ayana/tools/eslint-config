'use strict';

const path = require('path');

module.exports = {
	extends: path.join(__dirname, './base.js'),
	env: {
		browser: true,
	},
};
