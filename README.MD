# @ayana/eslint-config
> ESLint configuration for Ayana projects

## Install

```bash
yarn add @ayana/eslint-config
```

## Use

Add to `package.json`:

```json
{
	"name": "your-project",
	...
	"eslintConfig": {
		"extends": "@ayana"
	}
}
```
